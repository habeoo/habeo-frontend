export default [
    {
        id: 1,
        img: require('../../../assets/meditate.png'),
        title: 'Track your habits',
        description: 'Habeo helps you to improve your habits',
    },
    {
        id: 2,
        img: require('../../../assets/competition.png'),
        title: 'Competitions',
        description: 'Stay motivated and compete in bettering your habits with friends',
    },
    {
        id: 3,
        img: require('../../../assets/better-life.png'),
        title: 'Start today',
        description: 'Sign up to improve your life.',
    }
];
