import {
    Text,
    View,
    Animated,
    Easing,
} from 'react-native';
import React from 'react';
import {GoogleSocialButton} from "react-native-social-buttons";
import * as Google from "expo-auth-session/providers/google";
import {getCompetitor, getToken, getUser} from "../../api/user";
import {useDispatch} from "react-redux";
import {useToast} from "native-base";
import * as WebBrowser from "expo-web-browser";
import styles from './SlideItem.style';

WebBrowser.maybeCompleteAuthSession();

const SlideItem = ({item, navigation}) => {
    const translateYImage = new Animated.Value(40);
    const [accessToken, setAccessToken] = React.useState("");
    const dispatch = useDispatch();
    const toast = useToast()

    Animated.timing(translateYImage, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: true,
        easing: Easing.bounce,
    }).start();

    const [request, response, promptAsync] = Google.useAuthRequest({
        expoClientId: process.env.expoClientId,
        iosClientId: process.env.iosClientId,
        androidClientId: process.env.androidClientId,
        scopes: ['profile', 'email'],
    });

    React.useEffect(() => {
        if (response?.type === "success") {
            setAccessToken(response.authentication.accessToken);
            getNewToken(response.authentication.accessToken);
        }
    }, [response, accessToken]);

    async function getNewToken(accessToken) {
        getToken(accessToken)
            .then(() => {
                dispatch(getUser());
                dispatch(getCompetitor());
                navigation.navigate('Main');
            }).catch(err => {
            console.log(err)
            toast.show({
                description: err.message,
                placement: "top-right"
            })
        })
    }


    return (
        <View style={styles.container}>
            <Animated.Image
                source={item.img}
                resizeMode="contain"
                style={[
                    styles.image,
                    {
                        transform: [
                            {
                                translateY: translateYImage,
                            },
                        ],
                    },
                ]}
            />

            <View style={styles.content}>
                <Text style={styles.title}>{item.title}</Text>
                <Text style={styles.description}>{item.description}</Text>
                <Text style={styles.price}>{item.price}</Text>

                {item.id === 3 ? (
                    <View style={styles.textContainer}>
                        <GoogleSocialButton onPress={() => {
                            promptAsync();
                        }}/>
                    </View>
                ) : null}

            </View>
        </View>
    );
};

export default SlideItem;
