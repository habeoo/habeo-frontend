import React from "react";
import { View, Text, Image } from "react-native";
import styles from './HabitCard.style'

const HabitCard = ({ habit, index }) => {
    return (
        <View style={styles.card}>
            <View>
                <View style={{flex: 1, paddingLeft: 15 }}>
                    <Image
                        source={require('../../assets/habit-icon.png')}
                        style={[styles.cardImage, styles.shadowProp]}
                    />
                </View>
                <View style={styles.cardTitle}>
                    <Text style={{ fontWeight: "bold", fontSize: 16 }}>
                        {habit.name}
                    </Text>
                    <Text style={{ fontWeight: "bold", fontSize: 14, color: "grey" }}>
                        {habit.goal}
                    </Text>
                </View>
            </View>
        </View>
    );
}

export default HabitCard;
