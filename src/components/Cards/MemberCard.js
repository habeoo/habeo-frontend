import styles from "./GroupCard.style";
import {Text, View} from "react-native";
import {Icon} from "@rneui/base";
import {Avatar} from "@rneui/themed";

const MemberCard = ({member, rank}) => {
    return (
        <View style={[styles.shadowProp, {margin: 15}]}>
            <View style={{flexDirection: "row", alignItems: "center", alignSelf: "center"}}>
                {rank ? <Text style={{fontWeight: "bold", fontSize: 30, justifyContent: "center"}}>
                    {rank}
                </Text> : null}
                <View style={styles.cardTitle}>
                    <Avatar
                        size={50}
                        rounded
                        source={{ uri: member.photoUrl }}
                    />
                </View>
                <View style={{justifyContent: "center", marginLeft: 10}}>
                    <Text style={{fontWeight: "bold", fontSize: 16}}>
                        {member.name}
                    </Text>
                </View>
            </View>
        </View>
    )
}

export default MemberCard;
