import {FlatList, ScrollView, TouchableOpacity, View} from "react-native";
import {Icon, Text} from "@rneui/base";
import HabitCard from "./HabitCard";
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import CreateTagForm from "../Forms/TagForm";
import {deleteTag, getAllTags} from "../../api/tags";
import {useToast} from "native-base";
import styles from './TagCard.style';
import {getHabitById} from "../../api/habits";

const TagCard = ({navigation, tag, cardWidth}) => {
    const [activeCardIndex, setActiveCardIndex] = useState(0);
    const [activeEdit, setActiveEdit] = useState(false);
    const {habits} = useSelector((state) => state.habits);
    const toast = useToast()
    const dispatch = useDispatch()


    const filterHabitsByTag = () => {
        return habits?.filter(habit => habit.tag.id === tag.id);
    }

    const renderModal = () => {
        return <CreateTagForm pressed={true} tag={tag} pressCallback={pressCallback}/>;
    }

    const pressCallback = (pressed) => {
        console.log(pressed);
        setActiveEdit(!pressed);
    }

    return (
        <View style={styles.container}>
            <View style={{flexDirection: "row"}}>
                <View style={styles.textContainer}>
                    <Text h4 style={styles.title}>{tag.name}</Text>
                </View>
                <View style={{marginLeft: "auto", marginRight: 20, flexDirection: "row"}}>
                    <TouchableOpacity
                        onPress={() => setActiveEdit(!activeEdit)}>
                        {activeEdit ? renderModal() : null}
                        <Icon name="edit" color="gray"/>
                    </TouchableOpacity>
                    {/*<TouchableOpacity*/}
                    {/*    onPress={() => {*/}
                    {/*        deleteTag(tag.id).then(() => {*/}
                    {/*            toast.show({*/}
                    {/*                description: "Successfully deleted",*/}
                    {/*                placement: "top-right"*/}
                    {/*            });*/}
                    {/*            dispatch(getAllTags())*/}
                    {/*        });*/}
                    {/*    }}>*/}
                    {/*    <Icon name="delete" color="red"/>*/}
                    {/*</TouchableOpacity>*/}
                </View>
            </View>

            <FlatList
                onMomentumScrollEnd={(e) => {
                    setActiveCardIndex(
                        Math.round(e.nativeEvent.contentOffset.x / cardWidth)
                    );
                }}
                showsHorizontalScrollIndicator={false}
                data={filterHabitsByTag()}
                renderItem={({item, index}) => (
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => {
                            navigation.navigate("Habit", item.habit)
                            dispatch(getHabitById(item.habit.id))
                        }}
                    >
                        <HabitCard habit={item.habit}/>
                    </TouchableOpacity>
                )}
            />
        </View>
    );
}

export default TagCard;
