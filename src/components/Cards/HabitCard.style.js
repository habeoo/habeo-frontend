import {Dimensions, StyleSheet} from "react-native";

const { width } = Dimensions.get("screen");
const cardWidth = width / 1.5;

export default StyleSheet.create({
    card: {
        height: 90,
        width: cardWidth,
        elevation: 15,
        marginRight: "auto",
        marginLeft: "auto",
        borderRadius: 15,
        backgroundColor: "#a0ceff",
        marginBottom: 10
    },
    cardImage: {
        width: 70,
        height: cardWidth / 4,
        marginTop: 13,
        alignSelf: "flex-start",
        borderRadius: 15,
    },
    cardTitle: {
        flexDirection: "column",
        justifyContent: "space-between",
        alignItems: "flex-end",
        marginTop: 30,
        marginLeft: 8,
        marginRight: 8,
    },
    shadowProp: {
        shadowColor: "#171717",
        shadowOffset: { width: -2, height: 4 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
    },
});
