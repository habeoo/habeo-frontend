import {Dimensions, StyleSheet} from "react-native";

const {width} = Dimensions.get("screen");
const cardWidth = width / 1.4;

export default StyleSheet.create({
    card: {
        flex: 1,
        height: 60,
        backgroundColor: "#f6f6f6",
        width: cardWidth,
        marginRight: "auto",
        marginLeft: "auto",
        marginBottom: 15,
        borderRadius: 10,
        marginTop: 15,
        justifyContent: "center",
    },
    cardTitle: {
        flexDirection: "column",
        justifyContent: "center",
        marginLeft: 10
    },
});
