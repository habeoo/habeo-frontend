import {StyleSheet} from "react-native";

export default StyleSheet.create({
    container: {
        backgroundColor: "white",
        paddingBottom: 15,
        borderRadius: 20
    },
    textContainer: {
        paddingLeft: 22,
        marginBottom: 15
    },
    title: {
        fontSize: 20,
    },
    description: {
        color: "gray"
    },
    habit: {
        marginLeft: "auto",
        marginRight: "auto"
    }
});
