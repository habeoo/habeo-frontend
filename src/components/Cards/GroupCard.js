import {Text, View} from "react-native";
import styles from "./GroupCard.style";
import {Icon} from "@rneui/base";

const GroupCard = ({group}) => {
    return (
        <View style={[styles.card, styles.shadowProp]}>
            <View style={{flexDirection: "row"}}>
                <View style={styles.cardTitle}>
                    <Text style={{fontWeight: "bold", fontSize: 16}}>
                        {group.name}
                    </Text>
                    <Text style={{fontWeight: "bold", fontSize: 14, color: "grey"}}>
                        {group.description}
                    </Text>
                </View>
                <View style={{justifyContent: "center", marginLeft: "auto"}}>
                    <Icon name="chevron-right" />
                </View>
            </View>
        </View>
    )
}

export default GroupCard;
