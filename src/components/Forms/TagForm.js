import {Dialog} from "@rneui/themed";
import {useState} from "react";
import {Button, Icon, Input, Text} from "@rneui/base";
import {createNewTag, getAllTags, updateTag} from "../../api/tags";
import {useDispatch} from "react-redux";
import {ScrollView, StyleSheet, TouchableOpacity, View} from "react-native";
import {useToast} from "native-base";
import {isInputValid, validationErrorMessage} from "../../validation";

const CreateTagForm = ({tag, pressed, pressCallback}) => {
    const [name, setName] = useState(tag?.name ? tag.name : "");
    const [defaultTag, setDefaultTag] = useState(false);
    const dispatch = useDispatch();
    const [modalVisible, setModalVisible] = useState(pressed ? pressed : false);
    const toast = useToast();
    const [error, setError] = useState(null);

    return (
        <View>
            <Dialog
                isVisible={modalVisible}
                overlayStyle={{borderRadius: 20}}
            >
                <Dialog.Title title={tag ? "Edit tag" : "Create new tag"}/>

                <View>
                    <ScrollView>
                        <Input
                            placeholder='Name'
                            value={name}
                            onChangeText={text => {
                                if (!isInputValid(text)) {
                                    setError(validationErrorMessage);
                                } else {
                                    setName(text);
                                    setError(null);
                                }
                            }}
                        />
                        {error ? (<Text style={{color: 'red'}}>
                            {error}
                        </Text>) : null}
                    </ScrollView>
                </View>

                <Dialog.Actions>
                    {!error ? (
                        <Button
                            title={tag ? "Edit" : "Create"}
                            color="#45c44c"
                            containerStyle={styles.createButton}
                            onPress={() => {
                                tag ? updateTag({
                                        "id": tag.id,
                                        "name": name,
                                    }).then(() => {
                                        dispatch(getAllTags())
                                        toast.show({
                                            description: "Successfully edited",
                                            placement: "top-right"
                                        });
                                    })
                                    : dispatch(createNewTag({
                                        "name": name,
                                        "defaultTag": defaultTag
                                    })).then(() => {
                                        toast.show({
                                            description: "Successfully created",
                                            placement: "top-right"
                                        });
                                    });
                                setModalVisible(false)
                            }}
                        />
                    ) : null}
                    <Button
                        title="Cancel"
                        type="clear"
                        titleStyle={{color: "red"}}
                        containerStyle={{marginLeft: "auto"}}
                        onPress={() => {
                            setModalVisible(false);
                            pressCallback(modalVisible);
                        }}
                    />
                </Dialog.Actions>
            </Dialog>

        </View>

    )
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: "#e2c4f6",
        width: 40,
        height: 40,
        borderRadius: 50,
        justifyContent: "center",
        marginRight: 20
    },
    createButton: {
        borderRadius: 10,
    }
})

export default CreateTagForm;
