import {useToast} from "native-base";
import {ScrollView, StyleSheet, View} from "react-native";
import {Dialog} from "@rneui/themed";
import {Button, Input, Text} from "@rneui/base";
import {isInputValid, validationErrorMessage} from "../../validation";
import {useState} from "react";
import {addDailyEntryToHabit} from "../../api/habits";

const HabitProgressForm = ({pressed, pressCallback, habitEvent}) => {
    const [modalVisible, setModalVisible] = useState(pressed ? pressed : false);
    const [description, setDescription] = useState("");
    const toast = useToast();
    const [error, setError] = useState(null);

    return (
        <View>
            <Dialog
                isVisible={modalVisible}
                overlayStyle={{borderRadius: 20}}
            >
                <Dialog.Title title="Daily progress"/>

                <View>
                    <ScrollView>
                        <Input
                            placeholder='Description'
                            value={description}
                            onChangeText={text => {
                                if (!isInputValid(text)) {
                                    setError(validationErrorMessage);
                                } else {
                                    setDescription(text);
                                    setError(null);
                                }
                            }}
                        />
                        {error ? (<Text style={{color: 'red'}}>
                            {error}
                        </Text>) : null}
                    </ScrollView>
                </View>

                <Dialog.Actions>
                    {!error ? (
                        <Button
                            title="Post"
                            color="#45c44c"
                            containerStyle={styles.createButton}
                            onPress={() => {
                                addDailyEntryToHabit(habitEvent.id, description).then(() => {
                                    toast.show({
                                        description: "Daily entry saved",
                                        placement: "top-right"
                                    });
                                });
                                setModalVisible(false)
                                pressCallback(false);
                            }}
                        />
                    ) : null}
                    <Button
                        title="Cancel"
                        type="clear"
                        titleStyle={{color: "red"}}
                        containerStyle={{marginLeft: "auto"}}
                        onPress={() => {
                            setModalVisible(false);
                            pressCallback(false);
                        }}
                    />
                </Dialog.Actions>
            </Dialog>
        </View>
    )
}

export default HabitProgressForm;

const styles = StyleSheet.create({
    button: {
        backgroundColor: "#e2c4f6",
        width: 40,
        height: 40,
        borderRadius: 50,
        justifyContent: "center",
        marginRight: 20
    },
    createButton: {
        borderRadius: 10,
    }
})
