import {useDispatch} from "react-redux";
import {useToast} from "native-base";
import {useState} from "react";
import {Dialog} from "@rneui/themed";
import {ScrollView, View} from "react-native";
import {Button, Input, Text} from "@rneui/base";
import {createNewGroup, getCurrentGroup, getUserGroups, updateGroup} from "../../api/groupsApi";
import {isInputValid, validationErrorMessage} from "../../validation";

const GroupForm = ({pressed, pressCallback, groupData}) => {
    const dispatch = useDispatch();
    const [group, setGroup] = useState({
        name: groupData ? groupData.name : "",
        description: groupData ? groupData.description : ""
    });
    const [modalVisible, setModalVisible] = useState(pressed ? pressed : false);
    const toast = useToast();
    const [error, setError] = useState({
        descriptionError: null,
        nameError: null
    });

    const create = () => {
        createNewGroup(group).then(() => {
            toast.show({
                description: "Successfully created",
                placement: "top-right"
            });
        });
    }

    const edit = () => {
        updateGroup(groupData.id, group).then(() => {
            toast.show({
                description: "Successfully edited",
                placement: "top-right"
            });
            dispatch(getCurrentGroup(groupData.id));
            dispatch(getUserGroups());
        });
    }

    return (
        <Dialog
            isVisible={modalVisible}
            overlayStyle={{borderRadius: 20}}
        >
            <Dialog.Title title="Create new group"/>

            <View>
                <ScrollView>
                    <Input
                        placeholder='Name'
                        value={group.name}
                        onChangeText={text => {
                            if (!isInputValid(text)) {
                                setError({...error, nameError: validationErrorMessage});
                            } else {
                                setError({...error, nameError: null});
                                setGroup({...group, name: text})
                            }
                        }}
                    />
                    {error.nameError ? (<Text style={{color: 'red'}}>
                        {error.nameError}
                    </Text>) : null}
                    <Input
                        placeholder='Description'
                        value={group.description}
                        onChangeText={text => {
                            if (!isInputValid(text)) {
                                setError({...error, descriptionError: validationErrorMessage});
                            } else {
                                setError({...error, descriptionError: null});
                                setGroup({...group, description: text})
                            }
                        }}
                    />
                    {error.descriptionError ? (<Text style={{color: 'red'}}>
                        {error.descriptionError}
                    </Text>) : null}
                </ScrollView>
            </View>

            <Dialog.Actions>
                {!error.nameError && !error.descriptionError ? (
                    <Button
                        title={groupData ? "Edit" : "Create"}
                        color="#45c44c"
                        containerStyle={{borderRadius: 10}}
                        onPress={() => {
                            groupData ? edit() : create();
                            dispatch(getUserGroups());
                            setModalVisible(false);
                            pressCallback(false);
                        }}
                    />
                ) : null}
                <Button
                    title="Cancel"
                    type="clear"
                    titleStyle={{color: "red"}}
                    containerStyle={{marginLeft: "auto"}}
                    onPress={() => {
                        setModalVisible(false);
                        pressCallback(false);
                    }}
                />
            </Dialog.Actions>
        </Dialog>
    )
}

export default GroupForm;
