import {View} from "native-base";
import {Text, StyleSheet, TouchableOpacity} from "react-native";
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";

const SELECTED_COLOR = "#2d4c82";
const NON_SELECTED_COLOR = "white";
const DayOfTheWeekPicker = ({onChangeValue, disabled}) => {
    const {currentHabit} = useSelector(state => state.habits);

    const getHabitDaysFromCron = () => {
        let days = currentHabit?.cron.split(" ")[5];
        return days?.split(",").map(d => parseInt(d));
    }

    const [days, setDays] = useState([{
        name: "MONDAY",
        selected: false,
        index: 1,
    }, {
        name: "TUESDAY",
        selected: false,
        index: 2,
    }, {
        name: "WEDNESDAY",
        selected: false,
        index: 3,
    }, {
        name: "THURSDAY",
        selected: false,
        index: 4,
    }, {
        name: "FRIDAY",
        selected: false,
        index: 5,
    }, {
        name: "SATURDAY",
        selected: false,
        index: 6,
    }, {
        name: "SUNDAY",
        selected: false,
        index: 0,
    }]);

    useEffect(() => {
        let weekdays = getHabitDaysFromCron();
        if (weekdays) {
            setDays(prevDays => {
                return prevDays.map(day => {
                    if (weekdays.includes(day.index)) {
                        return {...day, selected: true};
                    } else {
                        return {...day, selected: false};
                    }
                });
            });
        }
    }, [currentHabit])

    return (
        <View style={{flexDirection: "row"}}>
            {days.map((day, i) => (
                <TouchableOpacity
                    onPress={() => {
                        days[i].selected = !days[i].selected;
                        onChangeValue(days.filter(day => day.selected).map(day => day.index))
                        setDays(days);
                    }}
                    disabled={disabled}
                    key={i}
                >
                    <View
                        style={[styles.dayButtonContainer, {backgroundColor: days[i].selected ? SELECTED_COLOR : NON_SELECTED_COLOR}]}>
                        <Text style={[styles.name, {color: days[i].selected ? "white" : "black"}]}>
                            {days[i].name.slice(0, 3)}</Text>
                    </View>

                </TouchableOpacity>
            ))}
        </View>
    )
}

export default DayOfTheWeekPicker;

const styles = StyleSheet.create({
    dayButtonContainer: {
        height: 33,
        width: 33,
        borderRadius: 6,
        justifyContent: "center",
        alignItems: "center"
    },
})
