import {useDispatch, useSelector} from "react-redux";
import {Button, Input, Text} from "@rneui/base";
import {StyleSheet, View} from "react-native";
import {Dialog} from "@rneui/themed";
import {useState} from "react";
import {createNewHabit, editHabit, getAllHabits, getHabitById, getTodaysHabits} from "../../api/habits";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import DayOfTheWeekPicker from "./Input/DayOfTheWeekPicker";
import {useToast} from "native-base";
import {isInputValid, isUnitValid, unitValidationErrorMessage, validationErrorMessage} from "../../validation";
import {addHabitToGroup, getCurrentGroup} from "../../api/groupsApi";
import {getTagById} from "../../api/tags";

const getWeekdaysFromCron = (cron) => {
    return cron.split(" ")[5].split(",").map(d => parseInt(d))
}

const getStartTimeFromCron = (cron) => {
    let splitedCron = cron.split(" ");
    return `${splitedCron[2]}:${splitedCron[1]}:${splitedCron[0]}`
}

const HabitForm = ({tag, habitSchedule, pressed, onPress, type, groupHabit}) => {
    const [habitType, setHabitType] = useState(habitSchedule ? habitSchedule.userHabit.habit.type : type);
    const [name, setName] = useState(habitSchedule ? habitSchedule.userHabit.habit.name : "");
    const [goal, setGoal] = useState(habitSchedule ? habitSchedule.userHabit.habit.goal : "");
    const [unit, setUnit] = useState(habitSchedule ? habitSchedule.userHabit.habit?.unit : "");
    const [target, setTarget] = useState(habitSchedule ? habitSchedule.userHabit.habit?.target : null);
    const [startTime, setStartTime] = useState(habitSchedule ? getStartTimeFromCron(habitSchedule.cron) : null);
    const dispatch = useDispatch();
    const [modalVisible, setModalVisible] = useState(pressed ? pressed : false);
    const [weekdays, setWeekdays] = useState(habitSchedule ? getWeekdaysFromCron(habitSchedule.cron) : [])
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [error, setError] = useState({
        goalError: null,
        nameError: null,
        unitError: null
    });
    const {currentGroup} = useSelector(state => state.groups);
    const toast = useToast();

    const handleStartConfirm = (time) => {
        setStartTime(new Date(time).toLocaleTimeString())
        console.log(startTime)
        setDatePickerVisibility(false)
    };

    const handleChangeDay = (days) => {
        setWeekdays(days)
        console.log("Days " + weekdays)
    }

    const clearProperties = () => {
        setStartTime("");
        setName("");
        setGoal("");
        setTarget("");
        setUnit("");
        setWeekdays([]);
    }

    const buildCron = () => {
        const time = startTime.split(":");
        return `0 ${time[1]} ${time[0]} * * ${weekdays}`;
    }

    const createHabitObject = () => {
        const habitObject = {
            "name": name,
            "goal": goal,
            "type": habitType,
            "cron": buildCron()
        };

        if (habitType === "measurable") {
            habitObject.unit = unit;
            habitObject.target = target;
        }

        if (!groupHabit) {
            habitObject.tagId = tag.id;
        } else {
            habitObject.tagId = null;
        }

        return habitObject;
    }


    const create = () => {
        const habit = createHabitObject();
        if (!groupHabit) {
            createNewHabit(habit).then(() => {
                toast.show({
                    description: "Successfully created",
                    placement: "top-right"
                });
                if (tag) dispatch(getTagById(tag.id));
                dispatch(getAllHabits());
            }).catch(err => {
                toast.show({
                    description: err.response.data.message,
                    placement: "top-right"
                });
            })
        } else {
            addHabitToGroup(currentGroup.id, habit).then(() => {
                toast.show({
                    description: "Successfully created",
                    placement: "top-right"
                });
                dispatch(getCurrentGroup(currentGroup.id))
            }).catch(err => {
                toast.show({
                    description: err.response.data.message,
                    placement: "top-right"
                });
            })
        }
    }

    const edit = () => {
        const habit = createHabitObject();
        console.log(`Habit: ${habit}`)
        const habitId = habitSchedule?.userHabit.habit.id;
        editHabit(habitId, habit).then(() => {
            toast.show({
                description: "Successfully edited",
                placement: "top-right"
            });
            dispatch(getHabitById(habitId))
        }).catch(err => {
            toast.show({
                description: err.response.data.message,
                placement: "top-right"
            });
        });
    }

    return (
        <>
            <Dialog
                isVisible={modalVisible}
                overlayStyle={{borderRadius: 20}}
            >
                <Dialog.Title title={habitSchedule ? "Edit habit" : "Create new habit"}/>

                <View>
                    <Input
                        placeholder='Name'
                        value={name}
                        onChangeText={text => {
                            setName(text)
                            if (!isInputValid(text)) {
                                setError({...error, nameError: validationErrorMessage});
                            } else {
                                setName(text);
                                setError({...error, nameError: null});
                            }
                        }}
                    />
                    {error.nameError ? (<Text style={{color: 'red'}}>
                        {error.nameError}
                    </Text>) : null}
                    <Input
                        placeholder='Goal'
                        value={goal}
                        onChangeText={text => {
                            setGoal(text)
                            if (!isInputValid(text)) {
                                setError({...error, goalError: validationErrorMessage});
                            } else {
                                setError({...error, goalError: null});
                                setGoal(text)
                            }
                        }}
                    />
                    {error.goalError ? (<Text style={{color: 'red'}}>
                        {error.goalError}
                    </Text>) : null}
                    {habitType === "measurable" ?
                        <>
                            <Input
                                placeholder='Unit'
                                value={unit}
                                onChangeText={text => {
                                    if (!isUnitValid(text)) {
                                        setError({...error, unitError: unitValidationErrorMessage})
                                    } else {
                                        setError({...error, unitError: null})
                                        setUnit(text)
                                    }
                                }}
                            />
                            {error.unitError ? (<Text style={{color: 'red'}}>
                                {error.unitError}
                            </Text>) : null}
                            <Input
                                placeholder='Target'
                                value={target}
                                onChangeText={text => setTarget(parseInt(text))}
                            />
                        </> : null}

                    <View style={{flexDirection: "row"}} onPress={() => setDatePickerVisibility(true)}>
                        <Input
                            placeholder='Start time'
                            value={startTime}
                            onPressIn={() => {
                                setDatePickerVisibility(true);
                            }}
                        >
                        </Input>
                        <DateTimePickerModal
                            isVisible={isDatePickerVisible}
                            mode="time"
                            onConfirm={handleStartConfirm}
                            onCancel={() => {
                                setDatePickerVisibility(false);
                            }}
                        />
                    </View>

                    <View style={styles.container}>
                        <DayOfTheWeekPicker weekdays={weekdays} onChangeValue={handleChangeDay}/>
                    </View>
                </View>

                <Dialog.Actions>
                    {!error.goalError && !error.nameError && !error.unitError ? (
                        <Button
                            title={habitSchedule ? "Edit" : "Create"}
                            color="#45c44c"
                            containerStyle={styles.createButton}
                            onPress={() => {
                                habitSchedule ? edit() : create()
                                dispatch(getAllHabits());
                                dispatch(getTodaysHabits());
                                setModalVisible(false);
                                onPress(false);
                            }}
                        />
                    ) : null}
                    <Button
                        title="Cancel"
                        type="clear"
                        titleStyle={{color: "red"}}
                        containerStyle={styles.clearButton}
                        onPress={() => {
                            setModalVisible(false);
                            onPress(false);
                            clearProperties();
                        }}/>
                </Dialog.Actions>
            </Dialog>
        </>
    )

}

const styles = StyleSheet.create({
    container: {
        paddingLeft: 10,
    },
    clearButton: {
        alignSelf: "center",
        marginLeft: "auto"
    },
    createButton: {
        borderRadius: 10,
    }
})

export default HabitForm;
