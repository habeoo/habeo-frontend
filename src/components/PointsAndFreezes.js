import {useDispatch, useSelector} from "react-redux";
import {Image, TouchableOpacity, View} from "react-native";
import {Button, Icon} from "@rneui/base";
import {Text, useToast} from "native-base";
import {Dialog} from "@rneui/themed";
import {useState} from "react";
import {buyFreeze} from "../api/habits";
import {getCompetitor, getUser} from "../api/user";
import styles from './PointsAndFreezes.style'

const PointsAndFreezes = () => {
    const {competitor} = useSelector(state => state.user)
    const [modalVisible, setModalVisible] = useState(false);
    const dispatch = useDispatch();
    const toast = useToast();

    return competitor ? (
        <View>
            <TouchableOpacity onPress={() => setModalVisible(true)}>
                <View style={styles.container}>
                    <Icon
                        color="yellow"
                        name='star'/>
                    <Text>{competitor.points}</Text>
                    <Icon
                        color="#B4CFFA"
                        name='ac-unit'/>
                    <Text>{competitor.freezes}</Text>
                </View>
            </TouchableOpacity>

            <Dialog
                isVisible={modalVisible}
            >
                <Dialog.Title title="Points and freezes"/>


                <View style={{flexDirection: "row", height: "auto"}}>
                    <Image
                        source={require('../assets/freeze.png')}
                        style={[styles.cardImage, styles.shadowProp]}
                    />
                    <Text style={{fontSize: 16, flex: 1, marginLeft: 15}}>
                        Protect your streak if you miss doing your habit for the day.
                    </Text>
                </View>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <Dialog.Actions>
                        <Button
                            title="Cancel"
                            type="clear"
                            titleStyle={{color: "red"}}
                            onPress={() => {
                                setModalVisible(false);
                            }}
                        />
                    </Dialog.Actions>
                    <View style={{flexDirection: "row", marginTop: 15, marginLeft: "auto"}}>
                        <View style={{alignItems: "center", flexDirection: "row", padding: 5}}>
                            <Icon
                                color="yellow"
                                name='star'/>
                            <Text>50</Text>
                        </View>
                        <Button
                            title="Buy"
                            color="#45c44c"
                            size="md"
                            titleStyle={{color: "white"}}
                            containerStyle={styles.createButton}
                            onPress={() => {
                                buyFreeze().then(() => {
                                    dispatch(getCompetitor());
                                    setModalVisible(false);
                                    toast.show({
                                        description: "Successfully bought",
                                        placement: "top-right"
                                    });
                                }).catch(err => {
                                    toast.show({
                                        description: err.response.data.message,
                                        placement: "top-right"
                                    });
                                })
                            }}/>
                    </View>
                </View>
            </Dialog>
        </View>
    ) : <View></View>
}

export default PointsAndFreezes;
