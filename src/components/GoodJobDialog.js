import {useEffect, useState} from "react";
import {Dialog, Image} from "@rneui/themed";
import {View} from "react-native";
import useSound from "use-sound";

const GoodJobDialog = (pressed, pressCallback) => {
    const [modalVisible, setModalVisible] = useState(pressed ? pressed : false);
    const successTrumpet = '../assets/success-trumpets.mp3';
    const [playActive] = useSound(
        successTrumpet,
        {volume: 0.25}
    );

    // useEffect(() => {
    //     setTimeout(() => {
    //         setModalVisible(false)
    //         pressCallback(modalVisible);
    //     }, 7000)
    // }, [])

    useEffect(() => {
        if (modalVisible) playActive();
    }, [modalVisible])

    return (
        <View>
            <Dialog
                isVisible={modalVisible}
                overlayStyle={{borderRadius: 20}}
            >
                <View>
                    <Image source={{uri: "https://giphy.com/embed/BrFuiMe3YUt3laSeEO"}}/>
                </View>
            </Dialog>
        </View>
    )
}

export default GoodJobDialog;
