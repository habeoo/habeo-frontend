import {useSelector} from "react-redux";
import {FlatList, TouchableOpacity} from "react-native";
import MemberCard from "../Cards/MemberCard";
import {ListItem} from "@rneui/themed";
import GroupMemberQuickActions from "../Buttons/GroupMemberQuickActions";

const GroupMembersList = () => {
    const {currentGroup} = useSelector(state => state.groups);
    return (
        <>
            {currentGroup ? (
                <FlatList
                    showsHorizontalScrollIndicator={false}
                    data={currentGroup?.members}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item}) => (
                        <ListItem.Swipeable
                            rightContent={() =>
                                GroupMemberQuickActions(item)
                            }
                            containerStyle={{
                                backgroundColor: "#f9f9f9",
                                justifyContent: "center"
                            }}
                        >
                            <TouchableOpacity
                                activeOpacity={1}
                            >
                                <MemberCard member={item}/>
                            </TouchableOpacity>
                        </ListItem.Swipeable>
                    )}
                />
            ) : null}
        </>
    )
}

export default GroupMembersList;
