import {useDispatch, useSelector} from "react-redux";
import {ListItem} from "@rneui/themed";
import HabitRightQuickActions from "../Buttons/HabitRightQuickActions";
import {TouchableOpacity, View} from "react-native";
import HabitCard from "../Cards/HabitCard";
import {getHabitById} from "../../api/habits";
import HabitLeftQuickActions from "../Buttons/HabitLeftQuickActions";

const PendingHabitsList = ({navigation}) => {
    const {todayHabits} = useSelector(state => state.habits);
    const dispatch = useDispatch();

    return (
        <View style={{flex: 1, paddingLeft: 10, paddingRight: 10}}>
            {todayHabits?.map(h => (
                <ListItem.Swipeable
                    rightContent={() =>
                        HabitRightQuickActions(h)
                    }
                    leftContent={() =>
                        HabitLeftQuickActions(h?.userHabit.habit.id)
                    }
                    containerStyle={{
                        backgroundColor: "#f9f9f9",
                        alignItems: "center",
                        justifyContent: "center"
                    }}
                >
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => {
                            navigation.navigate("Habit", h?.userHabit.habit)
                            dispatch(getHabitById(h?.userHabit.habit.id))
                        }}
                    >
                        <HabitCard habit={h?.userHabit.habit}/>
                    </TouchableOpacity>
                </ListItem.Swipeable>
            ))}
        </View>
    )
}

export default PendingHabitsList;
