export const actions = [
    {
        text: "Boolean",
        icon: null,
        name: "boolean",
        position: 2,
        buttonSize: 0.1,
        margin: -10,
        textStyle: {fontSize: 15}
    },
    {
        text: "Measurable",
        name: "measurable",
        icon: null,
        position: 1,
        buttonSize: 0.1,
        margin: -10,
        textStyle: {fontSize: 15}
    },
];
