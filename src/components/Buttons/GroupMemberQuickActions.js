import {useDispatch} from "react-redux";
import {useToast} from "native-base";
import {freezeHabit, getTodaysHabits} from "../../api/habits";
import {getUser} from "../../api/user";
import {Pressable, StyleSheet, View} from "react-native";
import {Icon} from "@rneui/base";
import GoodJobDialog from "../GoodJobDialog";
import {getCurrentGroup, getUserGroups, updateGroup} from "../../api/groupsApi";
import {useState} from "react";

const GroupMemberQuickActions = (member, group) => {
    const dispatch = useDispatch();
    const toast = useToast();
    const [modalVisible, setModalVisible] = useState(false);

    const deleteMember = () => {
        updateGroup(group.id, group).then(() => {
                dispatch(getUserGroups());
                dispatch(getCurrentGroup());
            }
        )
    }


    return (
        <View style={styles.qaContainer}>
            <View style={styles.button}>
                <Pressable style={{backgroundColor: "#b00000", borderRadius: 10, padding: 5}} onPress={() => {
                    deleteMember();
                }}>
                    <Icon
                        color="#ffffff"
                        name='delete'/>
                </Pressable>
            </View>
        </View>
    );
};

export default GroupMemberQuickActions;

const styles = StyleSheet.create({
    qaContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    button: {
        flex: 1,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
    },
})
