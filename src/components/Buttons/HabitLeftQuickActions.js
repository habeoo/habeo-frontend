import {useDispatch} from "react-redux";
import {useToast} from "native-base";
import {cancelHabit, getHabitEvents, getTodaysHabits} from "../../api/habits";
import {Pressable, StyleSheet, View} from "react-native";
import {Icon} from "@rneui/base";

const HabitLeftQuickActions = (qaItem) => {
    const dispatch = useDispatch();
    const toast = useToast();

    const cancel = () => {
        cancelHabit(qaItem.id).then(() => {
                toast.show({
                    description: "Habit canceled",
                    placement: "top-right"
                });
                dispatch(getTodaysHabits());
                dispatch(getHabitEvents(qaItem.id));
            }
        )
    }

    return (
        <View style={styles.qaContainer}>
            <View style={styles.button}>
                <Pressable style={{backgroundColor: "#b00000", borderRadius: 10, padding: 5}} onPress={() => {
                    cancel()
                }}>
                    <Icon
                        color="#ffffff"
                        name='close'/>
                </Pressable>
            </View>
        </View>
    );
}
export default HabitLeftQuickActions;

const styles = StyleSheet.create({
    qaContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    button: {
        flex: 1,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
    },
})
