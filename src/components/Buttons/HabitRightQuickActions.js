import {Pressable, Text, View, StyleSheet, Button} from "react-native";
import {freezeHabit, getDateHabitEvents, getTodaysHabits, markHabitAsDone} from "../../api/habits";
import {useDispatch} from "react-redux";
import {getUser} from "../../api/user";
import {Icon} from "@rneui/base";
import {useToast} from "native-base";
import {useState} from "react";
import HabitProgressForm from "../Forms/HabitProgressForm";

const HabitRightQuickActions = (qaItem) => {
    const dispatch = useDispatch();
    const toast = useToast();
    const [modalVisible, setModalVisible] = useState(false);

    const done = () => {
        console.log(qaItem.userHabit.habit)
        markHabitAsDone(qaItem.userHabit.habit.id).then(() => {
                dispatch(getDateHabitEvents(new Date().toISOString().split('T')[0]));
                dispatch(getUser());
            }
        )
    }

    const freeze = () => {
        freezeHabit(qaItem.userHabit.habit.id).then(() => {
            dispatch(getTodaysHabits());
            dispatch(getUser());
            toast.show({
                description: "Habit freezed",
                placement: "top-right"
            });
        })
    }

    const pressCallback = (pressed) => {
        setModalVisible(pressed);
    }

    return (
        <View style={styles.qaContainer}>
            <View style={styles.button}>
                <Pressable style={{backgroundColor: "#ff9e1b", borderRadius: 10, padding: 5}} onPress={() => setModalVisible(!modalVisible)}>
                    <Icon
                        color="#ffffff"
                        name='trending-up'/>
                    {modalVisible ?
                        <HabitProgressForm
                            habitEvent={qaItem}
                            pressCallback={pressCallback}
                            pressed={modalVisible}
                        /> : null}
                </Pressable>
            </View>
            <View style={styles.button}>
                <Pressable style={{backgroundColor: "#45c44c", borderRadius: 10, padding: 5}} onPress={() => {
                    done()
                }}>
                    <Icon
                        color="#ffffff"
                        name='check'/>
                </Pressable>
            </View>
            <View style={styles.button}>
                <Pressable style={{backgroundColor: "#b4cffa", borderRadius: 10, padding: 5}} onPress={() => freeze()}>
                    <Icon
                        color="#ffffff"
                        name='ac-unit'/>
                </Pressable>
            </View>
        </View>
    );
};

export default HabitRightQuickActions;

const styles = StyleSheet.create({
    qaContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    button: {
        flex: 1,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
    },
})
