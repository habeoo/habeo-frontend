import {StyleSheet, TouchableOpacity, View} from "react-native";
import {useDispatch} from "react-redux";
import {useEffect, useState} from "react";
import {Text} from "@rneui/base";
import {getDateHabitEvents} from "../api/habits";
import HabitsTabs from "./TabViews/HabitsTabs";

const Agenda = ({navigation}) => {
    const todayDate = new Date();
    const [week, setWeek] = useState([]);
    const dispatch = useDispatch();

    useEffect(() => {
        setWeek(getCurrentWeekDays())
    }, [])

    const getCurrentWeekDays = () => {
        let week = []
        for (let i = 1; i <= 7; i++) {
            let day = todayDate.getDate() - todayDate.getDay() + i;
            week.push(new Date(todayDate.setDate(day)));
        }
        return week;
    }

    return (
        <View style={{flex: 1}}>
            <View style={{flexDirection: "row", justifyContent: "center", alignSelf: "center", marginTop: 15}}>
                {week.map(day => (
                    <View style={{marginLeft: 5}}>
                        <TouchableOpacity onPress={() => {
                            dispatch(getDateHabitEvents(day.toISOString().slice(0, 10)))
                        }}>
                            <Text style={{
                                textAlign: "center",
                                color: "#b0b0b0"
                            }}>{day.toDateString().split(" ")[0]}</Text>
                            <View style={styles.circle}>
                                <Text style={styles.day}>{day.getDate()}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                ))}
            </View>
            <HabitsTabs navigation={navigation}/>
        </View>
    )
}

export default Agenda;

const styles = StyleSheet.create({
    circle: {
        borderRadius: 100,
        width: 40,
        height: 40,
    },
    timelineContainer: {
        backgroundColor: "#e3e3e3",
    },
    day: {
        textAlign: 'center',
        lineHeight: 40,
        fontSize: 15,
    }
})
