import {Dimensions, StyleSheet} from "react-native";

const {width} = Dimensions.get("screen");
const cardWidth = width / 1.3;

export default StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flexDirection: 'row',
        borderRadius: 15,
        justifyContent: "center",
        alignContent: "center",
        marginBottom: 5,
        padding: 3
    },
    cardImage: {
        height: cardWidth / 4,
        alignSelf: "flex-start",
    },
    cardTitle: {
        flexDirection: "column",
        justifyContent: "space-between",
        alignItems: "flex-end",
    },
    shadowProp: {
        shadowColor: "#171717",
        shadowOffset: {width: -2, height: 4},
        shadowOpacity: 0.2,
        shadowRadius: 3,
    },
    createButton: {
        borderRadius: 10,
    }
});
