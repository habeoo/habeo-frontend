import Timeline from "react-native-timeline-flatlist";
import {useSelector} from "react-redux";
import {useEffect, useState} from "react";

const HabitsTimeline = () => {
    const [timelineData, setTimelineData] = useState([]);
    const {todayHabits} = useSelector(state => state.habits);


    useEffect(() => {
        setTimelineData(getTimelineEvents(todayHabits));
    }, [todayHabits])

    const getTimelineEvents = (habits) => {
        console.log(`These are the habits: ${habits}`)
        return habits?.map(h => (
            {
                time: h?.start.split(" ")[1].substr(0, 5),
                title: h?.userHabit.habit.name,
                description: `${h?.userHabit.habit.goal} \n${h.status}`
            }
        ))
    }

    return (
        <>
            <Timeline
                data={timelineData}
                circleSize={15}
                circleColor='#2d4c82'
                lineColor='black'
                timeStyle={{
                    textAlign: 'center',
                    backgroundColor: '#a0ceff',
                    color: 'white',
                    padding: 5,
                    borderRadius: 13
                }}
                descriptionStyle={{color: 'gray'}}
                options={{
                    style: {paddingTop: 5, marginLeft: 10}
                }}
            />
        </>
    )
}

export default HabitsTimeline;
