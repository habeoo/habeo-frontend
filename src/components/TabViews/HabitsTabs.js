import {Tab, TabView} from "@rneui/themed";
import styles from "../../screens/Today.style";
import {View} from "react-native";
import HabitsTimeline from "../HabitsTimeline";
import PendingHabitsList from "../Lists/PendingHabitsList";
import {useEffect, useState} from "react";
import Timeline from "react-native-timeline-flatlist";
import {useSelector} from "react-redux";

const HabitsTabs = ({navigation}) => {
    const [index, setIndex] = useState(0);

    return (
        <>
            <Tab
                value={index}
                onChange={(i) => setIndex(i)}
                disableIndicator
                containerStyle={styles.tabItem}
            >
                <Tab.Item
                    title="Timeline"
                    titleStyle={{color: "black"}}
                    style={{backgroundColor: index === 0 ? "#f1f1f1" : "white"}}
                />
                <Tab.Item
                    title="Habits"
                    titleStyle={{color: "black"}}
                    style={{backgroundColor: index === 1 ? "#f1f1f1" : "white"}}
                />
            </Tab>
            <TabView
                value={index}
                onChange={(i) => setIndex(i)}
                disableSwipe={true}
            >
                <TabView.Item style={[styles.tabView, {borderTopEndRadius: 20}]}>
                    <HabitsTimeline/>
                </TabView.Item>
                <TabView.Item style={[styles.tabView, {borderTopStartRadius: 20}]}>
                    <PendingHabitsList navigation={navigation}/>
                </TabView.Item>
            </TabView>
        </>
    )
}

export default HabitsTabs;
