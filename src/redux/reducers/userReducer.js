import {createSlice} from "@reduxjs/toolkit";
import {getCompetitor, getUser} from "../../api/user";

const userSlice = createSlice({
    name: "users",
    initialState: {
        user: null,
        competitor: null
    },
    extraReducers: builder => {
        builder.addCase(getUser.fulfilled, (state, action) => {
            console.log(action.payload)
            state.user = action.payload
        })
        builder.addCase(getCompetitor.fulfilled, (state, action) => {
            state.competitor = action.payload;
        })
        builder.addCase(getCompetitor.rejected, (state, action) => {
            console.log(state)
        })
    }
})

export default userSlice.reducer;

