import {createSlice, current} from "@reduxjs/toolkit";
import {createNewTag, getAllTags, getTagById, updateTag} from "../../api/tags";

const tagSlice = createSlice({
    name: "tags",
    initialState: {
        tags: [],
        currentTag: null
    },
    extraReducers: builder => {
        builder.addCase(getAllTags.fulfilled, (state, action) => {
            state.tags = action.payload
        })

        builder.addCase(getTagById.fulfilled, (state, action) => {
            state.currentTag = action.payload;
        })

        builder.addCase(createNewTag.fulfilled, (state, action) => {
            state.tags.push(action.payload);
        })
    },
})

export default tagSlice.reducer;
