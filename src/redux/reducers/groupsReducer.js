import {createSlice} from "@reduxjs/toolkit";
import {getCurrentGroup, getGroupRanking, getUserGroups} from "../../api/groupsApi";

const groupsSlice = createSlice({
    name: "groups",
    initialState: {
        groups: [],
        currentGroup: null,
        currentGroupRecord: []
    },
    extraReducers: builder => {
        builder.addCase(getCurrentGroup.fulfilled, (state, action) => {
            state.currentGroup = action.payload;
        })
        builder.addCase(getUserGroups.fulfilled, (state, action) => {
            state.groups = action.payload;
        })
        builder.addCase(getGroupRanking.fulfilled, (state, action) => {
            state.currentGroupRecord = action.payload;
        })
    },
})

export default groupsSlice.reducer;
