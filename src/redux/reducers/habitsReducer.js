import {createSlice} from "@reduxjs/toolkit";
import {
    getAllHabits,
    getDateHabitEvents,
    getHabitById,
    getHabitEvents,
    getTodaysHabits,
    markHabitAsDone
} from "../../api/habits";

const habitSlice = createSlice({
    name: "habits",
    initialState: {
        todayHabits: [],
        habitEvents: [],
        habits: [],
        currentHabit: null
    },
    extraReducers: builder => {
        builder.addCase(getTodaysHabits.fulfilled, (state, action) => {
            state.todayHabits = action.payload;
        })

        builder.addCase(getHabitEvents.fulfilled, (state, action) => {
            state.habitEvents = action.payload;
        })

        builder.addCase(getAllHabits.fulfilled, (state, action) => {
            state.habits = action.payload;
        })
        builder.addCase(getDateHabitEvents.fulfilled, (state, action) => {
            state.todayHabits = action.payload;
        })
        builder.addCase(getHabitById.fulfilled, (state, action) => {
            state.currentHabit = action.payload;
        })
    },
})

export default habitSlice.reducer;
