import userReducer from "./reducers/userReducer";
import {configureStore} from "@reduxjs/toolkit";
import tagReducer from "./reducers/tagReducer";
import habitsReducer from "./reducers/habitsReducer";
import groupsReducer from "./reducers/groupsReducer";

const store = configureStore({
    reducer: {
        user: userReducer,
        tags: tagReducer,
        habits: habitsReducer,
        groups: groupsReducer
    }
})

export default store;
