import {FlatList, Linking, TouchableOpacity, View} from "react-native";
import styles from './Group.style'
import {Tab, TabView} from "@rneui/themed";
import {useEffect, useState} from "react";
import {Button, Icon, Text} from "@rneui/base";
import GroupMembersList from "../components/Lists/GroupMembersList";
import {FloatingAction} from "react-native-floating-action";
import {actions} from "../components/Buttons/data";
import HabitForm from "../components/Forms/HabitForm";
import {useDispatch, useSelector} from "react-redux";
import {getHabitById} from "../api/habits";
import HabitCard from "../components/Cards/HabitCard";
import GroupForm from "../components/Forms/GroupForm";
import {BASE_BACKEND_URL, GROUPS_URL} from "../constants";
import axios from "axios";
import MemberCard from "../components/Cards/MemberCard";

const Group = ({navigation}) => {
    const [index, setIndex] = useState(0);
    const [habitModalVisible, setModalVisible] = useState(false);
    const [groupModalVisible, setGroupModalVisible] = useState(false);
    const [habitType, setHabitType] = useState("");
    const {tags} = useSelector(state => state.tags);
    const {currentGroup} = useSelector(state => state.groups);
    const {currentGroupRecord} = useSelector(state => state.groups);
    const dispatch = useDispatch();

    // const handleDeepLink = async (event) => {
    //     if (event.url.path === 'habits/create') {
    //         const url = decodeURIComponent(event.url.path.queryParams.url);
    //         await axios.post(url);
    //     }
    // }
    //
    // useEffect(() => {
    //     Linking.addEventListener('url', handleDeepLink);
    //     return () => Linking.removeEventListener('url', handleDeepLink);
    // }, []);

    const getDefaultTag = () => {
        return tags.find(t => t.default)
    }

    const onPressHabitModal = (pressed) => {
        setModalVisible(pressed);
    }

    const onPressGroupModal = (pressed) => {
        setGroupModalVisible(pressed);
    }

    // const generateJoinLink = () => {
    //     const url = `${GROUPS_URL}${currentGroup.invitationUrl}`;
    //     return `habeo://habits/create?apiUrl=${encodeURIComponent(BASE_BACKEND_URL)}&url=${encodeURIComponent(url)}`;
    // }

    return (
        <View style={styles.container}>
            {currentGroup ? (
                <View style={[styles.innerContainer, {borderRadius: 15}]}>
                    <View style={{flexDirection: "row"}}>
                        <View style={{marginRight: "auto", justifyContent: "flex-start"}}>
                            <Button title="Back"
                                    type="clear"
                                    onPress={() => navigation.goBack()}
                            >
                                <Icon name="arrow-back-ios" size={17} color="rgb(55,146,222)"/>
                                Back
                            </Button>
                        </View>
                        <View style={{alignItems: "flex-end", alignSelf: "center", justifyContent: "center"}}>
                            <View style={{flexDirection: "row", marginRight: 20}}>
                                <View>
                                    <Text style={[styles.textHeader]}>{currentGroup?.name}</Text>
                                    <Text style={[styles.description]}>{currentGroup?.description}</Text>
                                </View>
                                <TouchableOpacity onPress={() => setGroupModalVisible(true)}>
                                    <Icon name="edit" color="orange"/>
                                </TouchableOpacity>
                                {groupModalVisible ?
                                    <GroupForm
                                        pressed={groupModalVisible}
                                        groupData={currentGroup}
                                        pressCallback={onPressGroupModal}
                                    /> : null}
                            </View>
                        </View>
                    </View>

                    <Tab
                        value={index}
                        onChange={(i) => setIndex(i)}
                        dense
                        disableIndicator
                        containerStyle={styles.tabItem}
                    >
                        <Tab.Item
                            title={<Icon name="view-agenda" />}
                            titleStyle={{color: "blue"}}
                            style={{backgroundColor: index === 0 ? "#f1f1f1" : "white"}}
                        />
                        <Tab.Item
                            title={<Icon name="military-tech" />}
                            titleStyle={{color: "black"}}
                            style={{backgroundColor: index === 1 ? "#f1f1f1" : "white"}}
                        />
                        <Tab.Item
                            title={<Icon name="groups" />}
                            titleStyle={{color: "black"}}
                            style={{backgroundColor: index === 2 ? "#f1f1f1" : "white"}}
                        />
                    </Tab>
                    <TabView
                        value={index}
                        onChange={(i) => setIndex(i)}
                    >
                        <TabView.Item style={[styles.tabView, {borderTopEndRadius: 20}]}>
                            <View style={{flex: 1, marginTop: 20}}>
                                <FlatList
                                    showsHorizontalScrollIndicator={false}
                                    data={currentGroup?.habits}
                                    renderItem={({item, index}) => (
                                        <TouchableOpacity
                                            activeOpacity={1}
                                            onPress={() => {
                                                navigation.navigate("Habit", item)
                                                dispatch(getHabitById(item.id))
                                            }}
                                        >
                                            <HabitCard habit={item}/>
                                        </TouchableOpacity>
                                    )}
                                />
                                <View style={{alignSelf: "flex-end"}}>
                                    <FloatingAction
                                        actions={actions}
                                        onPressItem={name => {
                                            setModalVisible(!habitModalVisible)
                                            setHabitType(name)
                                        }}
                                        buttonSize={40}
                                        color="#2d4c82"
                                    />
                                </View>

                                {habitModalVisible ?
                                    <HabitForm tag={getDefaultTag()} type={habitType} pressed={habitModalVisible}
                                               onPress={onPressHabitModal} groupHabit={true}/> : null}
                            </View>
                        </TabView.Item>
                        <TabView.Item style={[styles.tabView]}>
                            <View>
                                <FlatList
                                    showsHorizontalScrollIndicator={false}
                                    data={currentGroupRecord}
                                    renderItem={({item, index}) => (
                                        <MemberCard rank={index + 1} member={item}/>
                                    )}
                                />
                            </View>
                        </TabView.Item>
                        <TabView.Item style={[styles.tabView, {borderTopStartRadius: 20}]}>
                            <GroupMembersList/>
                        </TabView.Item>
                    </TabView>
                </View>
            ) : null}
        </View>
    )
}

export default Group;
