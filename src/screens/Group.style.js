import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#a0ceff',
        margin: 'auto',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20,
    },
    innerContainer: {
        flex: 1,
        backgroundColor: "#ffffff",
        borderRadius: 20,
        marginTop: 5,
        paddingTop: 20,
        marginBottom: 25,
    },
    header: {
        marginHorizontal: "auto",
        fontSize: 30,
        fontWeight: "bold"
    },
    textHeader: {
        fontSize: 24,
        fontWeight: "bold",
    },
    description: {
        fontSize: 17,
        marginTop: 5,
        marginBottom: 15,
        color: "#777777"
    },
    button: {
        backgroundColor: "#2d4c82",
        width: 40,
        height: 40,
        borderRadius: 50,
        justifyContent: "center",
        alignSelf: "flex-end",
        marginRight: 20,
        marginBottom: 30
    },
    tabItem: {
        flex: 1,
        borderTopEndRadius: 10,
        borderTopStartRadius: 10,
        marginLeft: 20,
        marginRight: 20
    },
    tabView: {
        flex: 1,
        backgroundColor: "#f9f9f9",
        marginLeft: 20,
        marginRight: 20,
    }
});

