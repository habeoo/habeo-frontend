import {View} from "native-base";
import React from "react";
import Slider from "../components/Carousel/Slider";

const Welcome = ({navigation}) => {
    return (
        <View>
            <Slider navigation={navigation}/>
        </View>
    )
}

export default Welcome;
