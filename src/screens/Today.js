import {View} from "react-native";
import {Text} from "@rneui/base";
import {useEffect} from "react";
import {useDispatch} from "react-redux";
import {getDateHabitEvents} from "../api/habits";
import styles from './Today.style'
import PointsAndFreezes from "../components/PointsAndFreezes";
import {getCompetitor} from "../api/user";
import Agenda from "../components/Agenda";


const Today = ({navigation}) => {
    const dispatch = useDispatch();
    const today = new Date();
    console.log(`TODAY: ${today}`);

    useEffect(() => {
        dispatch(getCompetitor())
        dispatch(getDateHabitEvents(today.toISOString().split('T')[0]));
    }, [])

    const getDateDayName = () => {
        const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        return daysOfWeek[today.getDay()];
    }

    console.log(`Before return in today ${today}`)
    return (
        <View style={{flex: 1}}>
            <View style={styles.container}>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <View style={{flexDirection: 'row'}}>
                        <View style={styles.circle}>
                            <Text style={styles.todayNumber}>{today.getDate()}</Text>
                        </View>
                        <Text style={styles.todayName}>{getDateDayName()}</Text>
                    </View>
                    <View style={{marginLeft: "auto"}}>
                        <PointsAndFreezes/>
                    </View>
                </View>

                <View style={styles.innerContainer}>
                    <Agenda navigation={navigation}/>
                </View>
            </View>
        </View>
    )
}

export default Today;
