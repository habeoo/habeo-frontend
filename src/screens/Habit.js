import {TouchableOpacity, View} from "react-native";
import {Button, Icon, Text} from "@rneui/base";
import {Avatar} from "@rneui/themed";
import DayOfTheWeekPicker from "../components/Forms/Input/DayOfTheWeekPicker";
import {Calendar} from "react-native-calendars";
import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getHabitEvents} from "../api/habits";
import styles from './Habit.style'
import HabitForm from "../components/Forms/HabitForm";

const Habit = ({navigation}) => {
    const today = new Date().toISOString().split('T')[0];
    const {habitEvents} = useSelector(state => state.habits);
    const {currentHabit} = useSelector(state => state.habits);
    const dispatch = useDispatch();
    const [modalVisible, setModalVisible] = useState(false);

    const onPressModal = (pressed) => {
        setModalVisible(pressed);
    }

    useEffect(() => {
        dispatch(getHabitEvents(currentHabit?.userHabit.habit.id));
    }, [])

    useEffect(() => {
        dispatch(getHabitEvents(currentHabit?.userHabit.habit.id));
    }, [currentHabit])

    const getAgendaDates = () => {
        const eventDates = habitEvents?.map(e => (e.start.substring(0, 10)))
        const obj = Object.fromEntries(eventDates.map(key => [key, {marked: true}]));
        return obj;
    }

    const getHabitProgress = () => {
        return habitEvents.find(e => e.start.split(" ")[0] === new Date().toISOString().split('T')[0])?.description
    }

    return (
        <View style={styles.container}>
            <View style={styles.innerContainer}>
                <View style={{flexDirection: "row"}}>
                    <View style={styles.button}>
                        <Button title="Back"
                                type="clear"
                                onPress={() => navigation.goBack()}
                        >
                            <Icon name="arrow-back-ios" size={17} color="rgb(55,146,222)"/>
                            Back
                        </Button>
                    </View>
                    <TouchableOpacity
                        style={{justifyContent: "center", marginLeft: 'auto', marginRight: 5}}
                        onPress={() => setModalVisible(!modalVisible)}
                    >
                        {modalVisible ?
                            <HabitForm
                                tag={currentHabit.userHabit.tag}
                                habitSchedule={currentHabit}
                                type={currentHabit.userHabit.habit.type}
                                pressed={true}
                                onPress={onPressModal}
                            />
                            : null}
                        <Icon name="edit" color="gray"/>
                    </TouchableOpacity>
                </View>
                {currentHabit ? (
                    <View>
                        <View style={styles.header}>
                            <View
                                style={{
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "flex-end",
                                    marginVertical: 5,
                                }}
                            >
                                <Avatar
                                    avatarStyle={{
                                        borderWidth: 1,
                                        borderColor: "#1E90FF",
                                    }}
                                    rounded
                                    size="large"
                                    source={require("../assets/habit-icon.png")}
                                />
                                <View style={{marginLeft: 8}}>
                                    <Text style={{fontSize: 20, fontWeight: "bold", marginTop: 16}}>
                                        {currentHabit?.userHabit.habit.name}
                                    </Text>
                                    <Text
                                        style={{
                                            fontSize: 14,
                                            fontWeight: "bold",
                                            color: "grey",
                                            paddingBottom: 5,
                                        }}
                                    >
                                        {currentHabit?.userHabit.habit.goal}
                                    </Text>
                                </View>
                            </View>
                        </View>

                        <View style={styles.description}>
                            <Text>Today's progress: {getHabitProgress()}</Text>
                            {currentHabit?.userHabit.habit.target ? (
                                <Text>
                                    {"\u2022"} {currentHabit?.userHabit.habit?.target} {currentHabit?.userHabit.habit?.unit}
                                </Text>
                            ) : null}
                            <Text>
                                Frequency:
                            </Text>
                            <DayOfTheWeekPicker disabled={true}/>
                            {habitEvents ? (
                                <Calendar
                                    initialDate={today}
                                    markedDates={
                                        getAgendaDates()
                                    }
                                />
                            ) : null}
                        </View>
                    </View>
                ) : null}
            </View>
        </View>
    )
}

export default Habit;
