import {Dimensions, ScrollView, StyleSheet, View} from "react-native";
import TagCard from "../components/Cards/TagCard";
import {Button, Icon} from "@rneui/base";
import HabitForm from "../components/Forms/HabitForm";
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import Spinner from 'react-native-loading-spinner-overlay';
import {getTagById} from "../api/tags";
import styles from './Tag.style'
import {actions} from "../components/Buttons/data";
import {FloatingAction} from "react-native-floating-action";

const Tag = ({route, navigation}) => {
    const tag = useSelector(state => state.tags.currentTag);
    const {width} = Dimensions.get("screen");
    const cardWidth = width / 1.9;
    const dispatch = useDispatch();
    const [spinner, setSpinner] = useState(true);
    const [modalVisible, setModalVisible] = useState(false);
    const [habitType, setHabitType] = useState("");

    useEffect(() => {
        dispatch(getTagById(route.params.id))
        console.log(tag)
        setSpinner(false);
    }, [])

    const onPressModal = (pressed) => {
        setModalVisible(pressed);
    }

    return (
        <>
            {tag ? (
                    <View style={styles.container}>
                        <View style={styles.innerContainer}>
                            <View style={styles.button}>
                                <Button title="Back"
                                        type="clear"
                                        onPress={() => navigation.goBack("Habits")}
                                >
                                    <Icon name="arrow-back-ios" size={17} color="rgb(55,146,222)"/>
                                    Back
                                </Button>
                            </View>
                            <ScrollView style={{borderRadius: 20}}>
                                <TagCard tag={tag} cardWidth={cardWidth} navigation={navigation}/>
                            </ScrollView>

                            <View style={{justifyContent: "flex-end"}}>
                                <FloatingAction
                                    actions={actions}
                                    onPressItem={name => {
                                        setModalVisible(!modalVisible)
                                        setHabitType(name)
                                    }}
                                    buttonSize={40}
                                    color="#2d4c82"
                                />
                                {modalVisible ?
                                    <HabitForm
                                        tag={tag}
                                        type={habitType}
                                        pressed={modalVisible}
                                        onPress={onPressModal}
                                        groupHabit={false}
                                    /> : null}
                            </View>
                        </View>
                    </View>
                ) :
                (
                    <Spinner
                        visible={spinner}
                        textContent={'Loading...'}
                    />
                )}
        </>
    )
}

export default Tag;
