import styles from './Group.style'
import {FlatList, TouchableOpacity, View} from "react-native";
import PointsAndFreezes from "../components/PointsAndFreezes";
import {Icon, Text} from "@rneui/base";
import {useEffect, useState} from "react";
import GroupForm from "../components/Forms/GroupForm";
import {useDispatch, useSelector} from "react-redux";
import {getCurrentGroup, getGroupRanking, getUserGroups} from "../api/groupsApi";
import GroupCard from "../components/Cards/GroupCard";

const Groups = ({navigation}) => {
    const [activeCreate, setActiveCreate] = useState(false);
    const {groups} = useSelector(state => state.groups);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getUserGroups())
    }, [])

    const pressCallback = (pressed) => {
        setActiveCreate(!pressed);
    }

    return (
        <View style={styles.container}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
                <View style={{marginRight: "auto"}}>
                    <Text style={styles.header}>Groups</Text>
                </View>
                <View style={{marginLeft: "auto"}}>
                    <PointsAndFreezes/>
                </View>
            </View>
            <View style={styles.innerContainer}>
                <FlatList
                    showsHorizontalScrollIndicator={false}
                    data={groups}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item}) => (
                        <TouchableOpacity
                            activeOpacity={1}
                            onPress={() => {
                                navigation.navigate("Group", item);
                                dispatch(getCurrentGroup(item.id));
                                dispatch(getGroupRanking(item.id))
                            }}
                        >
                            <GroupCard group={item} />
                        </TouchableOpacity>
                    )}
                />
                <TouchableOpacity style={[styles.button, {alignItems: "center"}]} onPress={() => setActiveCreate(true)}>
                    <Icon name="add" color="white"/>
                    {activeCreate ? <GroupForm pressed={true} pressCallback={pressCallback}/> : null}
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Groups;
