import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#a0ceff',
        margin: 'auto',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20
    },
    innerContainer: {
        flex: 1,
        backgroundColor: "white",
        borderRadius: 20,
        marginLeft: 10,
        marginRight: 10,
        paddingTop: 25,
        marginBottom: 25
    },
    header: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginHorizontal: 20,
        alignItems: "center",
    },
    textHeader: {
        fontSize: 24,
        fontWeight: "bold",
    },
    description: {
        fontSize: 15,
        marginTop: 15,
        marginLeft: 20,
    },
    button: {
        flexDirection: "row",
        paddingLeft: 5,
    }
});

