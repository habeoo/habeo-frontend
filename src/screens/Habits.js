import {useDispatch, useSelector} from "react-redux";
import {Dimensions, FlatList, TouchableOpacity, View} from 'react-native';
import {Icon, SearchBar, Text} from "@rneui/base";
import {useEffect, useState} from "react";
import TagCard from "../components/Cards/TagCard";
import CreateTagForm from "../components/Forms/TagForm";
import {ScrollView} from "native-base";
import HabitCard from "../components/Cards/HabitCard";
import PointsAndFreezes from "../components/PointsAndFreezes";
import {getAllHabits, getHabitById} from "../api/habits";
import {getAllTags} from "../api/tags";
import styles from './Habits.style'


const Habits = ({navigation}) => {
    const {habits} = useSelector((state) => state.habits);
    const {tags} = useSelector(state => state.tags);
    const [search, setSearch] = useState("");
    const [filteredDataSource, setFilteredDataSource] = useState([]);
    const [masterDataSource, setMasterDataSource] = useState([]);
    const [activeCreate, setActiveCreate] = useState(false);
    const dispatch = useDispatch();

    const {width} = Dimensions.get("screen");
    const cardWidth = width / 1.9;

    useEffect(() => {
        dispatch(getAllHabits());
        dispatch(getAllTags());
        console.log(masterDataSource)
        console.log(filteredDataSource)
    }, [])

    useEffect(() => {
        setMasterDataSource(habits);
        setFilteredDataSource(habits);
    }, [habits])


    const searchFilterFunction = (text) => {
        // Check if searched text is not blank
        if (text) {
            // Inserted text is not blank
            // Filter the masterDataSource
            // Update FilteredDataSource
            const newData = masterDataSource.filter((item) => {
                const itemData = `${item.habit.name}`
                    ? `${item.habit.name}`.toUpperCase()
                    : "".toUpperCase();
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1;
            });
            setFilteredDataSource(newData);
            setSearch(text);
        } else {
            // Inserted text is blank
            // Update FilteredDataSource with masterDataSource
            setFilteredDataSource(masterDataSource);
            setSearch(text);
        }
    };

    const pressCallback = (pressed) => {
        console.log(pressed);
        setActiveCreate(!pressed);
    }

    return (
        <View style={styles.container}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
                <Text h2 style={styles.title}>Habits</Text>
                <View style={{marginLeft: "auto"}}>
                    <PointsAndFreezes/>
                </View>
            </View>
            <View style={{flex: 1, backgroundColor: "white", borderRadius: 20}}>
                <ScrollView stickyHeaderIndices={[0]} style={{borderRadius: 20}}>
                    <View style={{
                        borderRadius: 20
                    }}>
                        <SearchBar
                            round
                            searchIcon={{size: 26}}
                            containerStyle={styles.searchContainer}
                            inputContainerStyle={styles.searchInputContainer}
                            placeholder="Search habit"
                            onChangeText={(text) => searchFilterFunction(text)}
                            onClear={(text) => searchFilterFunction("")}
                            value={search}
                        />
                    </View>
                    <View style={{flex: 1}}>
                        {tags ? (
                            <View style={{
                                justifyContent: "space-between",
                                borderRadius: 20,
                                paddingTop: 10,
                            }}
                            >
                                {search !== "" ? (
                                    <FlatList
                                        showsHorizontalScrollIndicator={false}
                                        data={filteredDataSource}
                                        keyExtractor={(item, index) => index.toString()}
                                        renderItem={({item}) => (
                                            <TouchableOpacity
                                                activeOpacity={1}
                                                onPress={() => {
                                                    dispatch(getHabitById(item.habit.id))
                                                    navigation.navigate("Habit", item.habit)
                                                }}
                                            >
                                                <HabitCard habit={item.habit}/>
                                            </TouchableOpacity>
                                        )}
                                    />
                                ) : (
                                    <FlatList
                                        showsHorizontalScrollIndicator={false}
                                        data={tags}
                                        keyExtractor={(item, index) => index.toString()}
                                        renderItem={({item}) => (
                                            <TouchableOpacity
                                                activeOpacity={1}
                                                onPress={() => {
                                                    navigation.navigate("Tag", item)
                                                }}
                                            >
                                                <TagCard tag={item} cardWidth={cardWidth} navigation={navigation}/>
                                            </TouchableOpacity>
                                        )}
                                    />
                                )}
                            </View>
                        ) : null}
                        <TouchableOpacity style={styles.button} onPress={() => setActiveCreate(true)}>
                            <Icon name="add" color="white"/>
                            {activeCreate ? <CreateTagForm pressed={true} pressCallback={pressCallback}/> : null}
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        </View>
    );
}

export default Habits;
