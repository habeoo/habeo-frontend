import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#a0ceff',
        margin: 'auto',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20
    },
    title: {
        fontWeight: 'bold',
        // color: "white"
    },
    tagTitle: {
        fontSize: 16,
    },
    tagDescritpion: {
        color: 'gray'
    },
    divider: {
        borderTop: 3,
        borderStyle: 'solid',
    },
    emptyTag: {
        display: 'inline'
    },
    searchContainer: {
        flex: 1,
        fontSize: 16,
        paddingLeft: "auto",
        paddingRight: "auto",
        borderRadius: 20,
        paddingTop: 20,
        backgroundColor: "white",
        borderBottomColor: "transparent",
        borderTopColor: "transparent",
    },
    searchInputContainer: {
        backgroundColor: "#F0F8FF",
        marginLeft: 10,
        marginRight: 10,
    },
    button: {
        backgroundColor: "#2d4c82",
        width: 40,
        height: 40,
        borderRadius: 50,
        justifyContent: "center",
        alignSelf: "flex-end",
        marginRight: 20,
    }
});
