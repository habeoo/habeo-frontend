import {StyleSheet} from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#a0ceff',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20
    },
    innerContainer: {
        flex: 1,
        backgroundColor: "white",
        borderRadius: 20,
        marginTop: 10
    },
    title: {
        fontWeight: 'bold',
    },
    timeline: {
        paddingTop: 15,
    },
    heading: {
        alignItems: "center",
        paddingBottom: 15,
    },
    qaContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    button: {
        width: 80,
        alignItems: 'center',
        justifyContent: 'center',
    },
    circle: {
        backgroundColor: '#2d4c82',
        borderRadius: 100,
        width: 45,
        height: 45,
    },
    todayNumber: {
        textAlign: 'center',
        lineHeight: 45,
        fontSize: 20,
        fontWeight: "bold",
        color: "white"
    },
    todayName: {
        lineHeight: 45,
        fontSize: 20,
        fontWeight: "bold",
        color: "#2d4c82",
        marginLeft: 10
    },
    tabItem: {
        borderTopEndRadius: 10,
        borderTopStartRadius: 10,
        marginLeft: 20,
        marginRight: 20,
    },
    tabView: {
        flex: 1,
        backgroundColor: "#f9f9f9",
        marginLeft: 20,
        marginRight: 20,
    }
});
