export const BASE_BACKEND_URL = 'https://habeo.app/';
// export const BASE_BACKEND_URL = 'http://localhost:8080/';

export const GOOGLE_AUTH_URL = BASE_BACKEND_URL + 'google/signin/';
export const USER_URL = BASE_BACKEND_URL + 'user';
export const COMPETITOR_URL = BASE_BACKEND_URL + 'competitor';

export const TAGS_URL = BASE_BACKEND_URL + 'tags/';

export const HABITS_URL = BASE_BACKEND_URL + 'habits/';

export const GROUPS_URL = BASE_BACKEND_URL + 'groups/'
