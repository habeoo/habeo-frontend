import axios from "axios";
import {COMPETITOR_URL, GOOGLE_AUTH_URL, USER_URL} from "../constants";
import {createAsyncThunk} from "@reduxjs/toolkit";

export const getToken = async (accessToken) => {
    return await axios.post(GOOGLE_AUTH_URL + accessToken)
        .then(res => {
            axios.defaults.headers.common['Authorization'] = "Bearer " + res.data;
            return res.data;
        });
}

export const getUser = createAsyncThunk('users/saveUser', async () => {
    return await axios.get(USER_URL).then(res => {
        return res.data;
    })
});

export const getCompetitor = createAsyncThunk('users/saveCompetitor', async () => {
    return await axios.get(COMPETITOR_URL).then(res => {
        console.log(res.data)
        return res.data;
    })
})
