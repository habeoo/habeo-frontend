import {TAGS_URL} from "../constants";
import axios from "axios";
import {createAsyncThunk} from "@reduxjs/toolkit";

export const getAllTags = createAsyncThunk('tags/saveAllTags', async () => {
    return await axios.get(TAGS_URL).then(res => {
        console.log(res.data)
        return res.data
    });
});

export const getTagById = createAsyncThunk('tags/getTag', async (tagId) => {
    return await axios.get(`${TAGS_URL}${tagId}`).then(res => {
        console.log(`${TAGS_URL}${tagId}`);
        console.log(res.data)
        return res.data
    });
});

export const createNewTag = createAsyncThunk('tags/saveNewTag', async (tag) => {
    return await axios.post(TAGS_URL, tag).then(res => {
        return res.data;
    });
});

export const deleteTag = async (tagId) => {
    return await axios.delete(`${TAGS_URL}${tagId}`)
}

export const updateTag = async (tag) => {
    return await axios.put(`${TAGS_URL}${tag.id}`, tag).then(res => {
        return res.data;
    })
}
