import {createAsyncThunk} from "@reduxjs/toolkit";
import axios from "axios";
import {GROUPS_URL} from "../constants";

export const createNewGroup = async (groupData) => {
    return await axios.post(`${GROUPS_URL}`, groupData).then(res => {
        return res.data;
    })
}

export const updateGroup = async (groupId, groupData) => {
    return await axios.put(`${GROUPS_URL}${groupId}`, groupData).then(res => {
        return res.data;
    })
}

export const addHabitToGroup = async (groupId, habit) => {
    return await axios.post(`${GROUPS_URL}${groupId}/habits`, habit).then(res => {
        return res.data;
    })
}

export const getUserGroups = createAsyncThunk('groups/getUserGroups', async () => {
    return await axios.get(GROUPS_URL).then(res => {
        return res.data;
    })
})

export const getCurrentGroup = createAsyncThunk('groups/getCurrentGroup', async (groupId) => {
    return await axios.get(`${GROUPS_URL}${groupId}`).then(res => {
        return res.data;
    })
})

export const getGroupRanking = createAsyncThunk('groups/getGroupRanking', async (groupId) => {
    return await axios.get(`${GROUPS_URL}${groupId}/ranking`).then(res => {
        return res.data;
    })
})
