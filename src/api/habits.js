import {createAsyncThunk} from "@reduxjs/toolkit";
import axios from "axios";
import {BASE_BACKEND_URL, HABITS_URL, TAGS_URL} from "../constants";

export const createNewHabit = async (habit) => {
    return await axios.post(`${HABITS_URL}`, habit).then(res => {
        res.data;
    })
}

export const editHabit = async (habitId, habit) => {
    return await axios.put(`${HABITS_URL}${habitId}`, habit).then(res => {
        res.data;
    })
}

export const cancelHabit = async (habitId) => {
    await axios.patch(`${HABITS_URL}${habitId}/cancel`);
}

export const addDailyEntryToHabit = async (eventId, description) => {
    await axios.patch(`${HABITS_URL}events/${eventId}`, description);
}

export const getAllHabits = createAsyncThunk('habits/saveAllHabits', async () => {
    return await axios.get(`${HABITS_URL}`).then(res => {
        return res.data;
    })
})

export const getHabitById = createAsyncThunk('habits/saveCurrentHabit', async (habitId) => {
    return await axios.get(`${HABITS_URL}${habitId}`).then(res => {
        return res.data;
    })
})

export const getTodaysHabits = createAsyncThunk('habits/getTodaysHabits', async () => {
    return await axios.get(`${HABITS_URL}today`).then(res => {
        return res.data
    });
});

export const getDateHabitEvents = createAsyncThunk('habits/getDateHabitEvents', async (date) => {
    return await axios.get(`${HABITS_URL}events/${date}`).then(res => {
        return res.data
    });
})

export const getHabitEvents = createAsyncThunk('habits/getHabitEvents', async (habitId) => {
    return await axios.get(`${HABITS_URL}${habitId}/events`).then(res => {
        return res.data;
    })
})

export const markHabitAsDone = async(habitId) => {
    return await axios.post(`${BASE_BACKEND_URL}habits/${habitId}/done`).then(res => {
        return res.data;
    })
}

export const freezeHabit = async(habitId) => {
    return await axios.post(`${BASE_BACKEND_URL}habits/${habitId}/freeze`).then(res => {
        return res.data;
    })
}

export const buyFreeze = async() => {
    return await axios.post(`${BASE_BACKEND_URL}freeze/new`);
}

