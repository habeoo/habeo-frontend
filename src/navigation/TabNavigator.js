import Today from "../screens/Today";
import Habits from "../screens/Habits";
import {
    Animated,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import {CurvedBottomBar} from 'react-native-curved-bottom-bar';
import {Icon} from "@rneui/base";
import Groups from "../screens/Groups";
import Group from "../screens/Group";

const TabNavigator = () => {
    const _renderIcon = (routeName, selectedTab) => {
        let icon = '';

        switch (routeName) {
            case 'Today':
                icon = 'today';
                break;
            case 'Habits':
                icon = 'view-agenda';
                break;
            case 'Groups':
                icon = 'groups';
                break;
        }

        return (
            <Icon
                name={icon}
                size={25}
                color={routeName === selectedTab ? '#2d4c82' : 'gray'}
            />
        );
    };

    const renderTabBar = ({routeName, selectedTab, navigate}) => {
        return (
            <TouchableOpacity
                onPress={() => navigate(routeName)}
                style={styles.tabbarItem}
            >
                {_renderIcon(routeName, selectedTab)}
            </TouchableOpacity>
        );
    };

    return (
        <CurvedBottomBar.Navigator
            type="DOWN"
            style={styles.bottomBar}
            shadowStyle={styles.shawdow}
            height={55}
            circleWidth={50}
            bgColor="white"
            initialRouteName="Today"
            borderTopLeftRight
            screenOptions={{headerShown: false}}
            renderCircle={({selectedTab, navigate}) => (
                <Animated.View style={[styles.btnCircleUp, {backgroundColor: selectedTab === "Today" ? '#2d4c82' : '#f6f6f6'}]}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => navigate("Today")}
                    >
                        <Icon name={'today'} color={selectedTab === "Today" ? "white" : "gray"} size={25}/>
                    </TouchableOpacity>
                </Animated.View>
            )}
            tabBar={renderTabBar}
        >
            <CurvedBottomBar.Screen
                name="Groups"
                component={Groups}
                position="LEFT"
            />
            <CurvedBottomBar.Screen
                name="Today"
                component={Today}
                position="CIRCLE"
            />
            <CurvedBottomBar.Screen
                name="Habits"
                component={Habits}
                position="RIGHT"
            />
            <CurvedBottomBar.Screen
                name="Group"
                component={Group}
            />
        </CurvedBottomBar.Navigator>
    );
};

export default TabNavigator;

const styles = StyleSheet.create({
    closeButtonContainer: {
        height: 40,
        width: 40,
        justifyContent: 'center',
        alignSelf: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 5
        },
        elevation: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        borderRadius: 6,
    },

    container: {
        flex: 1,
        padding: 20,
    },
    shawdow: {
        shadowColor: '#DDDDDD',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 1,
        shadowRadius: 5,
    },
    button: {
        flex: 1,
        justifyContent: 'center',
    },
    bottomBar: {},
    btnCircleUp: {
        width: 60,
        height: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        bottom: 18,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 1,
    },
    imgCircle: {
        width: 30,
        height: 30,
        tintColor: 'gray',
    },
    tabbarItem: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    img: {
        width: 30,
        height: 30,
    }
})

