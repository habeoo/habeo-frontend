import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import Welcome from "../screens/Welcome";
import Habit from "../screens/Habit";
import Tag from "../screens/Tag";
import TabNavigator from "./TabNavigator";

const Stack = createStackNavigator();

export default function Navigation() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{
                headerShown: false
            }}>
                <Stack.Screen name="Welcome" component={Welcome}/>
                <Stack.Screen name="Main" component={TabNavigator}/>
                <Stack.Screen name="Tag" component={Tag}/>
                <Stack.Screen name="Habit" component={Habit}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}
