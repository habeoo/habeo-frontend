export const textFieldRegex = /^[a-zA-Z,.\s]*$/

export const unitFieldRegex = /^[a-z/\s]*$/

export const isInputValid = (text) => {
    return textFieldRegex.test(text);
};

export const isUnitValid = (text) => {
    return unitFieldRegex.test(text);
}

export const unitValidationErrorMessage = "Only lowercase letters, space and slash allowed";
export const validationErrorMessage = "Only letters, comma, dot and space allowed";
