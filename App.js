import Navigation from "./src/navigation";
import {NativeBaseProvider} from "native-base/src/core/NativeBaseProvider";
import {BASE_BACKEND_URL} from "./src/constants";
import axios from "axios";
import store from "./src/redux/store";
import {Provider} from "react-redux";

axios.defaults.baseURL = BASE_BACKEND_URL;
export default function App() {
  return (
      <Provider store={store}>
          <NativeBaseProvider>
            <Navigation />
          </NativeBaseProvider>
      </Provider>
  );
}
